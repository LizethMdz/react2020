import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';
import { useFetchGift } from '../../hooks/useFetchGifs';
import { renderHook } from '@testing-library/react-hooks'


describe('Pruebas en useFetchGifs', () => {
    test('Debe de retornar el esrado inicial', async () => {
        const { result, waitForNextUpdate } = renderHook(() => useFetchGift('goku'));
        const { data: images, loading } = result.current;
        //console.log(images, loading);
        await waitForNextUpdate();
        expect(images).toEqual([]);
        expect(loading).toBe(true);
    });
    test('Debe de retornar un arreglo de imgs y el loading en false', async () => {
        const { result, waitForNextUpdate } = renderHook(() => useFetchGift('goku'));
        await waitForNextUpdate();
        const { data: images, loading } = result.current;

        expect(images.length).toEqual(10);
        expect(loading).toBe(false);
    });
});