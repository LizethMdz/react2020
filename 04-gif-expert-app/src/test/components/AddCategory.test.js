import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';

import { AddCategory } from '../../components/AddCategory';

describe('Pruebas en el componente GifGridItem', () => {

    const setCategories = jest.fn(); // Me permite hacer referencia a como funcionaria esa funcion realmente
    let wrapper = shallow(<AddCategory setCategorias={setCategories} />);

    beforeEach(() => {
        // Clear all instances and calls to constructor and all methods:
        jest.clearAllMocks();
        wrapper = shallow(<AddCategory setCategorias={setCategories} />);
    });

    test('Debe mostrar GifGridItem correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    });

    // Simular el llennado de un input
    test('Debe de cambiar la caja de texto', () => {
        const input = wrapper.find('input');
        const value = 'Hola';
        input.simulate('change', { target: { value } });
        expect(wrapper.find('p').text().trim()).toBe(value);
    });

    test('No Debe de postear la información del submit', () => {
        wrapper.find('form').simulate('submit', { preventDefault() { } });
        expect(setCategories).not.toHaveBeenCalled();
    });

    test('Debe de postear la información del submit', () => {
        const input = wrapper.find('input');
        const value = 'mundo';
        // Simular el llenado
        input.simulate('change', { target: { value } });
        // Validar el llenado
        //expect(wrapper.find('p').text().trim()).toBe(value);
        // Simular el submit
        wrapper.find('form').simulate('submit', { preventDefault() { } });
        // Validar el submit
        expect(setCategories).toHaveBeenCalled();
        expect(setCategories).toHaveBeenCalledWith(expect.any(Function));
        // Valor debe estar vacío
        expect(input.text()).toBe('');
    });

});