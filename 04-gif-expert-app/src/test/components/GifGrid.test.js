import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';

import { GifGrid } from '../../components/GifGrid';
import { useFetchGift } from '../../hooks/useFetchGifs';

// No sayuda a simular las funciones que estan en otro directorio
jest.mock('../../hooks/useFetchGifs');

describe('Pruebas en el componente GifGrid', () => {
    const category = 'goku';
    test('Debe mostrar GifGrid correctamente', () => {
        useFetchGift.mockReturnValue({
            data: [],
            loading: true
        });
        const wrapper = shallow(<GifGrid category={category} />);
        expect(wrapper).toMatchSnapshot();
    });

    test('Debe mostrar items cuando se cargan imágenes correctamente (useFetchGifs)', () => {
        // Simulando la data
        const imgs = [{
            id: 'abc',
            url: 'http://test.jpg',
            title: 'Test'

        }];
        useFetchGift.mockReturnValue({
            data: imgs,
            loading: false
        });
        const wrapper = shallow(<GifGrid category={category} />);
        expect(wrapper).toMatchSnapshot(); // Crearía una copia directa en el snapshot
        expect(wrapper.find('p').exists()).toBe(false);
        // Traer el componente que nos trae la data
        expect(wrapper.find('GifGridItem').length).toBe(imgs.length);
    });


});