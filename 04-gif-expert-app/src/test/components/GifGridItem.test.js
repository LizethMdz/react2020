import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';

import { GifGridItem } from '../../components/GifGridItem';


describe('Pruebas en el componente GifGridItem', () => {
    const data = {
        title: 'peces',
        url: 'http://peces.com'
    }
    const wrapper = shallow(<GifGridItem {...data} />);

    test('Debe mostrar GifGridItem correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    });

    test('Debe tener un párrafo con el title', () => {
        const p = wrapper.find('p').text().trim();
        expect(p).toBe(data.title);
    });

    test('Debe tener la imagen igual al url y alt de los props', () => {
        const img = wrapper.find('img');
        // Hay una propiedad que nos ayuda a traer
        // todas las propiedades del elemento html.
        expect(img.prop('src')).toBe(data.url);
        expect(img.prop('alt')).toBe(data.title);
    });
    test('Debe tener la clse animate__fadeIn', () => {
        const div = wrapper.find('div').hasClass('animate__fadeIn');
        // Hay una propiedad que nos ayuda a traer
        // todas las clases del elemento html.
        // cont clase = div.prop('className');
        //   expect(div.include('animate__fadeIn')).toBe(true);
        expect(div).toBe(true);
    });
});