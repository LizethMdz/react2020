import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';

import GifExpertApp from '../GifExpertApp';

describe('Pruebas en el componente GifGridItem', () => {
    test('Debe mostrar el GifExpertApp correctamente', () => {
        const wrapper = shallow(<GifExpertApp />);
        expect(wrapper).toMatchSnapshot();
    });
    test('Debe mostrar una lista de categorias', () => {
        const categories = ['goku', 'one punsh'];
        const wrapper = shallow(<GifExpertApp defaultCategories={categories} />);
        expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('GifGrid').length).toBe(categories.length);
    });

});