import '@testing-library/jest-dom';
import { getGifs } from '../../helpers/getGifs';

describe('Pruebas en el componente getGifs Fetch', () => {
    test('Debe mostrar GifGridItem correctamente', async () => {
        const gifs = await getGifs('Goku');
        expect(gifs.length).toBe(10);
    });
});