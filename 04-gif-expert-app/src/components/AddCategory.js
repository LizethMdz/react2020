import React, { useState } from 'react';
import PropTypes from 'prop-types';
export const AddCategory = ({ setCategorias }) => {
    const [inputValue, setInputValue] = useState('');
    const handleInputChange = (e) => {
        setInputValue(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (inputValue.trim().length > 2) {
            setCategorias(cats => [inputValue, ...cats,]);
            setInputValue('');
        }
    }

    return (
        <div className="form__box">
            <p>{inputValue}</p>
            <form onSubmit={handleSubmit}>
                <input
                    className="form__text"
                    type="text"
                    value={inputValue}
                    onChange={handleInputChange}
                />
            </form>
        </div>
    )
}

AddCategory.propTypes = {
    setCategorias: PropTypes.func.isRequired
}

