import React from 'react'
import { GifGridItem } from './GifGridItem';
import { useFetchGift } from '../hooks/useFetchGifs';
import PropTypes from 'prop-types';

export const GifGrid = ({ category }) => {

    const { data: images, loading } = useFetchGift(category);

    return (
        <div className="gifGrid__container__card-grid">
            <div className="gifGrid__container__card-title">
                <h2 className="animate__animated animate__fadeIn">{category}</h2>
            </div>
            <br />
            { loading && <p className="animate__animated animate__flash">Cargando...</p>}
            <br />
            {images.map(img =>
                (
                    <GifGridItem
                        key={img.id}
                        {...img}
                    />
                ))
            }
        </div>
    )
}

GifGrid.propTypes = {
    category: PropTypes.string.isRequired
}
