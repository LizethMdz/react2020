import React, { useState } from 'react'
import { AddCategory } from './components/AddCategory';
import { GifGrid } from './components/GifGrid';
const GifExpertApp = ({ defaultCategories = [] }) => {
    //const categorias = ['One Push', 'Dragon Ball', 'Animales'];
    const [categorias, setCategorias] = useState(defaultCategories);
    //const [categorias, setCategorias] = useState(['Animales']);
    // const coches = 'Coches';
    // const handleAdd = () => {
    //     setCategorias((categoria) => [...categoria, coches]);
    //     //setCategorias([...categorias, coches]);
    // }

    return (
        <>
            <h2>GiftExpertApp</h2>
            <hr />

            <AddCategory setCategorias={setCategorias} />

            {/* <button onClick={handleAdd}>Agregar</button> */}

            <ol>
                {categorias.map((category) =>
                    (
                        <GifGrid
                            key={category}
                            category={category}
                        />
                    ))
                }
            </ol>
        </>
    )
}

export default GifExpertApp
