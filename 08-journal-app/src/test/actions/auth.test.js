import configureStore from 'redux-mock-store' //ES6 modules
import thunk from 'redux-thunk';
import { db } from '../../firebase/firebase-config';
import { login, logout, startLogout, startLoginEmailPassword } from '../../actions/auth';
import { types } from '../../types/types';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

const initState = {};
let store = mockStore(initState);


describe('Probar auth action', () => {

    beforeEach(() => {
        store = mockStore(initState);
    });

    test('Login y logut deben de crear la ccion respectiva', async () => {
        const uid = 'ABC123';
        const displayName = 'Fernando';
        await store.dispatch(login(uid, displayName));
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: types.login,
            payload: {
                uid,
                displayName
            }
        });
    });

    test('debe de realizar el startLogout', async () => {

        await store.dispatch(startLogout());

        const actions = store.getActions();

        expect(actions[0]).toEqual({
            type: types.logout
        });

        expect(actions[1]).toEqual({
            type: types.notesLogoutCleaning
        });


    });

    test('debe de iniciar el startLoginEmailPassword', async () => {

        await store.dispatch(startLoginEmailPassword('test@mail.com', '123456'));

        const actions = store.getActions();

        expect(actions[1]).toEqual({
            type: types.login,
            payload: {
                uid: '5cCIIw1p3oTELi4qTmLvi7md9wC3',
                displayName: null
            }
        });

    });
});