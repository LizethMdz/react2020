import { authReducer } from '../../reducers/authReducer';
import { types } from '../../types/types';


describe('Pruebas en authReducer', () => {

    const initialState = {};

    test('Debe de realizar el login', () => {

        const usuario = {
            uid: 'dsadkjsa',
            displayName: 'test'
        };

        const action = {
            type: types.login,
            payload: usuario
        }
        const state = authReducer(initialState, action);
        expect(state).toEqual({
            uid: 'dsadkjsa',
            name: 'test'
        });
    });

    test('Debe de realizar el logout', () => {
        const usuario = {
            uid: 'dsadkjsa',
            name: 'test'
        };

        const action = {
            type: types.logout
        }
        const state = authReducer(usuario, action);
        expect(state).toEqual({});
    });
    test('No debe de realizar cambios en el state', () => {
        const usuario = {
            uid: 'dsadkjsa',
            name: 'test'
        };

        const action = {
            type: 'dsadsadsa'
        }
        const state = authReducer(usuario, action);
        expect(state).toEqual(usuario);
    });

});