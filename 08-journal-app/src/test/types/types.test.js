const { types } = require("../../types/types");

describe('Pruebas en los types', () => {
    const types_output = {
        login: '[Auth] Login',
        logout: '[Auth] Logout',

        uiSetError: '[UI] Set Error',
        uiRemoveError: '[UI] Remove Error',

        uiStartLoading: '[UI] Start Loading',
        uiFinishLoading: '[UI] Finish Loading',

        notesAddNew: '[Notes] New note',
        notesActive: '[Notes] Set active note',
        notesLoad: '[Notes] Load notes',
        notesUpdated: '[Notes] Updated note',
        notesFileUrl: '[Notes] Updated image url',
        notesDelete: '[Notes] Delete note',
        notesLogoutCleaning: '[Notes] Logout Cleaning',
    }
    test('Debe de retorna un objeto de types', () => {
        expect(types).toEqual(types_output);
    });
});