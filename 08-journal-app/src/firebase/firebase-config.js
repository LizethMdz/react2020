import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: process.env.REACT_APP_APIKEY,
    authDomain: process.env.REACT_APP_AUTHDOMAIN,
    databaseURL: process.env.REACT_APP_DATABASEURL,
    projectId: process.env.REACT_APP_PROJECTID,
    storageBucket: process.env.REACT_APP_STORAGEBUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGINGSENDERID,
    appId: process.env.REACT_APP_APPID,
};

// const firebaseConfigTesting = {
//     apiKey: "AIzaSyBsLTfiq027Dd6V9_Eaavpq1r2ilix7Uwk",
//     authDomain: "redux-demo-956a9.firebaseapp.com",
//     databaseURL: "https://redux-demo-956a9.firebaseio.com",
//     projectId: "redux-demo-956a9",
//     storageBucket: "redux-demo-956a9.appspot.com",
//     messagingSenderId: "737564835325",
//     appId: "1:737564835325:web:aba097f609993414d3ed94"
// };

//console.log(process.env);                              


// if (process.env.NODE_ENV === 'test') {
//     // testing
//     firebase.initializeApp(firebaseConfigTesting);
// } else {
//     //dev/prod
//     firebase.initializeApp(firebaseConfig);
// }

firebase.initializeApp(firebaseConfig);

const db = firebase.firestore();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export {
    db, googleAuthProvider, firebase
}