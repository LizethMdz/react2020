import { types } from "../types/types";

/**Acciones ejecutadas antes de que vayan a Reducer */

export const setError = (err) => ({
    type: types.uiSetError,
    payload: err
});

export const removeError = () => ({
    type: types.uiRemoveError
});

export const startLoading = () => ({
    type: types.uiStartLoading,
    payload: true
});

export const finishLoading = () => ({
    type: types.uiFinishLoading,
    payload: false
});