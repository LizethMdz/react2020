import React from 'react'
import { authReducer } from '../../auth/authReducer';
import { types } from '../../types/types';


describe('Pruebas en authReducer', () => {


    test('Debe de retornar el estado por defecto', () => {
        const state = authReducer({ logged: false }, {});
        expect(state).toEqual({ logged: false });
    });
    test('Debe de autenticar y colocar el name del usuario', () => {

        const action = {
            payload: {
                name: 'lizeth',
            },
            type: types.login
        }

        const state = authReducer({ logged: false }, action);

        expect(state).toEqual({ name: 'lizeth', logged: true });
    });
    test('Debe de borrar el name del usuario y logged en false', () => {
        const action = {
            type: types.logout
        }

        const state = authReducer({ name: 'lizeth', logged: true }, action);

        expect(state).toEqual({ logged: false });
    });
});
