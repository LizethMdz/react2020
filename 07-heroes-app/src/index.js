import React from 'react';
import ReactDOM from 'react-dom';
import { HeroesApp } from './HeroresApp';
ReactDOM.render(

  <HeroesApp />,
  document.getElementById('root')
);

