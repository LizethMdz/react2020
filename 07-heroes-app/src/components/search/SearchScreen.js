import React, { useMemo } from 'react'
import { useForm } from '../../hooks/useForm';
import { HeroCard } from '../heroes/HeroCard';
import queryString from 'query-string';
import { useLocation } from 'react-router-dom';
import { getHeroesByName } from '../../selectors/getHeroesByName';

export const SearchScreen = ({ history }) => {


    const location = useLocation();
    const { q = '' } = queryString.parse(location.search);


    const [value, handleInputChange] = useForm({
        name: q
    });

    const { name } = value;

    // Solo llamar la busqueda cuando cambie el name
    const heroesFilter = useMemo(() => getHeroesByName(q), [q]);
    //console.log(name);

    const handleSearch = (e) => {
        e.preventDefault();
        history.push(`?q=${name}`)
        //console.log(value);
    }

    return (
        <div>
            <h2>Search Screen</h2>
            <hr />
            <div className="row">
                <div className="col-5">
                    <h4>Busqueda</h4>
                    <form onSubmit={handleSearch}>
                        <input
                            type="text"
                            name="name"
                            autoComplete="off"
                            value={name}
                            placeholder="Encuentra tu heroe"
                            onChange={handleInputChange}
                            className=" form-control" />

                        <button
                            type="submit"
                            className="btn mt-3 btn-outline-primary btn-block">
                            Buscar
                        </button>
                    </form>
                </div>
                <div className="col-7">
                    <h4>Resultados</h4>
                    <hr />
                    {
                        (q === '') &&
                        <div className="alert alert-info" role="alert">
                            <h6 className="alert-heading">Busca un heroe..</h6>
                        </div>
                    }
                    {
                        (q !== '' && heroesFilter.length === 0) &&
                        <div className="alert alert-danger" role="alert">
                            <h6 className="alert-heading">No hay un heroe válido {q} !</h6>
                        </div>
                    }


                    {
                        heroesFilter.map(heroe => (
                            <HeroCard
                                key={heroe.id}
                                {...heroe}>
                            </HeroCard>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}
