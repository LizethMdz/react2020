import React from 'react'
import { HeroList } from '../heroes/HeroList'

export const DcScreen = () => {
    return (
        <div className="container mt-3">
            <h2>DC Characters</h2>
            <hr />
            <HeroList publisher="DC Comics"></HeroList>
        </div>
    )
}
