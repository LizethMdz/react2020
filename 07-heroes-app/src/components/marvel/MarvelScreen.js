import React from 'react'
import { HeroList } from '../heroes/HeroList'

export const MarvelScreen = () => {



    return (
        <div className="container mt-3">
            <h2>Marvel Characters</h2>
            <hr />
            <HeroList publisher="Marvel Comics"></HeroList>
        </div>
    )
}
