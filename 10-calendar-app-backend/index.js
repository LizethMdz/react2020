const express = require('express');
require('dotenv').config();
const cors = require('cors');
const { dbConnection } = require('./database/config');
const app = express();

//Directorio Público - Middleware
app.use(express.static('public'));
// Lectura y parseo de body
app.use(express.json());

//Rutas
// TODO auth / crear/login/renew
app.use('/api/auth', require('./routes/auth.route'));
app.use('/api/events', require('./routes/events.route'));

// Data base
//mern_user
//mTmjBsOrVk0ebuMG
dbConnection();

//CORS
app.use(cors());



// Escuchar las peticiones
app.listen(process.env.PORT, () => {
    console.log(`Servidor corriendo en puerto ${process.env.PORT}`)
});