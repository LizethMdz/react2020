import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import Swal from 'sweetalert2';
import '@testing-library/jest-dom';
import { startChecking, startLogin, startRegister } from '../../actions/auth';
import { types } from '../../types/types';
import * as fetchModule from '../../helpers/fetch';

jest.mock('sweetalert2', () => ({
    fire: jest.fn()
}));

const middlewares = [thunk];

const mockStore = configureStore(middlewares);

const initialState = {}

let store = mockStore(initialState);

let token = '';
Storage.prototype.setItem = jest.fn();

describe('Pruebas en el Auth actions', () => {
    beforeEach(() => {
        store = mockStore(initialState);
        jest.clearAllMocks();
    });

    test('startLogin correcto', async () => {
        await store.dispatch(startLogin('liz@mail.com', '123456'));
        const actions = store.getActions();

        expect(actions[0]).toEqual({
            type: types.authLogin,
            payload: {
                uid: expect.any(String),
                name: expect.any(String)
            }
        });

        expect(localStorage.setItem).toHaveBeenCalledWith('token', expect.any(String));
        expect(localStorage.setItem).toHaveBeenCalledWith('token-init-date', expect.any(Number));
        // Saber con que argumentos fue llamado la funcion jest.fn()
        token = localStorage.setItem.mock.calls[0][1];
        // console.log(localStorage.setItem.mock.calls[0][1])
    });

    test('startLogin incorrecto', async () => {

        await store.dispatch(startLogin('liz@mail.com', '123456789'));
        let actions = store.getActions();

        expect(actions).toEqual([]);
        expect(Swal.fire).toHaveBeenCalledWith('Error', 'Password incorrecto', 'error');

        await store.dispatch(startLogin('liz@mail2.com', '123456'));
        actions = store.getActions();

        expect(Swal.fire).toHaveBeenCalledWith('Error', 'Este usuario no existe con ese correo', 'error');

    });

    test('startRegister correcto', async () => {

        // SIMULAMOS LA LLAMADA DEL FETCH SIN TOKEN
        fetchModule.fetchSinToken = jest.fn(() => ({
            json() {
                return {
                    ok: true,
                    uid: '123',
                    name: 'carlos',
                    token: 'ABC123ABC123'
                }
            }
        }));

        await store.dispatch(startRegister('test2@test.com', '123456', 'test'));

        const actions = store.getActions();

        expect(actions[0]).toEqual({
            type: types.authLogin,
            payload: {
                uid: '123',
                name: 'carlos'
            }
        })

        expect(localStorage.setItem).toHaveBeenCalledWith('token', 'ABC123ABC123');
        expect(localStorage.setItem).toHaveBeenCalledWith('token-init-date', expect.any(Number));



    });

    test('startChecking correcto', async () => {
        fetchModule.fetchConToken = jest.fn(() => ({
            json() {
                return {
                    ok: true,
                    uid: '123',
                    name: 'carlos',
                    token: 'ABC123ABC123'
                }
            }
        }));


        await store.dispatch(startChecking());

        const actions = store.getActions();

        expect(actions[0]).toEqual({
            type: types.authLogin,
            payload: {
                uid: '123',
                name: 'carlos'
            }
        });


        expect(localStorage.setItem).toHaveBeenCalledWith('token', 'ABC123ABC123');
    });


});