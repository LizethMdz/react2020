import { authReducer } from "../../reducers/authReducer";
import { types } from "../../types/types";

const initialstate = {
    checking: true,
}

describe('Pruebas en authReducer.js', () => {
    test('debe de retornar el estado por defecto', () => {
        const state = authReducer(initialstate, {});
        expect(state).toBe(initialstate);
    });

    test('Debe de autenticar el usuario', () => {
        const action = {
            type: types.authLogin,
            payload: {
                uid: 'abc123',
                name: 'carlos'
            }
        }

        const state = authReducer(initialstate, action);
        expect(state).toStrictEqual({
            checking: false, uid: 'abc123', name: 'carlos'
        });


    });
});