const { fetchSinToken, fetchConToken } = require("../../helpers/fetch");

describe('Pruebas en el helper fetch', () => {

    let token = '';

    test('fetchSinToken debe de funcionar', async () => {
        const resp = await fetchSinToken('auth', { email: 'liz@mail.com', password: '123456' }, 'POST');
        expect(resp instanceof Response).toBe(true);

        const body = await resp.json();
        expect(body.ok).toBe(true);

        token = body.token;
    });

    test('fetchConToken debe de funcionar', async () => {
        localStorage.setItem('token', token);
        const resp = await fetchConToken('events/5fac3dba1fd1343a0cdd98b3', {}, 'DELETE');
        const body = await resp.json();
        //console.log(body);

        expect(body.ok).toBe(false);
        expect(body.msg).toBe('Evento no existe por ese id');


    });
});