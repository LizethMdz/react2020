export const types = {
    uiOpenModal: '[ui] Open Modal',
    uiCloseModal: '[ui] Close Modal',

    eventSetActive: '[event] Set Active',
    eventLogout: '[event] Logout event',
    eventAddNew: '[event] Add New',
    eventLoaded: '[event] Event Loaded',
    eventClearActiveEvent: '[event] Clear Active Event',
    eventUpdated: '[event] Event Updated',
    eventDeleted: '[event] Event Deleted',

    authCheckingFinish: '[auth] Finish checking login state',
    authStartLogin: '[auth] Start login',
    authLogin: '[auth] Login',
    authStartRegister: '[auth] Start Register',
    authStartStartTokenRenew: '[auth] Start token renew',
    authLogout: '[auth] Logout',

} 