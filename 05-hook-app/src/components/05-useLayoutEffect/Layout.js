import { useFetch } from "../../hooks/useFetch";
import { useCounter } from '../../hooks/useCounter';
import { useLayoutEffect, useRef, useState } from "react";

export const Layout = () => {
    const { counter, increment } = useCounter(1);
    const { data } = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`);
    // !!null Transforma el data a un false, porque si es !null = true
    const { quote } = !!data && data[0];

    const pTag = useRef();
    const [boxSize, setBoxSize] = useState({});

    // Despues de la carga de una referencia a un elemento en el html
    useLayoutEffect(() => {
        // `current` apunta al elemento de entrada de texto montado
        setBoxSize(pTag.current.getBoundingClientRect());
    }, [quote]);

    return (
        <div className="container">
            <div className="row mt-3">
                <div className="col-md-8">
                    <h1>BreakingBad Quotes - LayoutEffect</h1>
                    <hr />


                    <blockquote className="d-flex text-right">
                        <p
                            className="mb-0"
                            ref={pTag}
                        >
                            {quote}
                        </p>
                    </blockquote>

                    <pre>
                        {JSON.stringify(boxSize, null, 3)}
                    </pre>



                    <button
                        className="btn btn-primary"
                        onClick={increment}
                    >
                        Siguiente quote
            </button>

                </div>
            </div>
        </div>
    )

}