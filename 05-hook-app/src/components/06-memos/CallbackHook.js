import React, { useState, useCallback, useEffect } from 'react';
import { ShowIncrement } from './ShowIncrement';
export const CallbackHook = () => {

    const [counter, setCounter] = useState(10);
    // De esta forma la funcion redibujaría el dom
    // const increment = () => {
    //     setCounter(counter + 1);
    // }

    // Will return a memoized version of the callback that only changes if one of the inputs has changed.
    const increment = useCallback((num) => {
        setCounter(c => c + num);
    }, [setCounter]);

    // Accepts a function that contains imperative, possibly effectful code.
    useEffect(() => {
        // ???
    }, [increment])


    return (
        <div className="container">
            <div className="row mt-3">
                <div className="col-md-8">
                    <h1>useCallback Hook:  {counter}  </h1>
                    <hr />
                    <ShowIncrement increment={increment} />
                </div>
            </div>
        </div>
    )
}