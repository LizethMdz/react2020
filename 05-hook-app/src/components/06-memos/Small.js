import React from 'react'

export const Small = React.memo(({ value }) => {
    // Solo se redibujará el si su property cambió
    // De lo contrario no se redibujará, pero utilizará
    // la versión memorizada del estado del value
    console.log(' Me volví a llamar :(  ');
    // http?

    return (
        <small> { value} </small>
    )
});
