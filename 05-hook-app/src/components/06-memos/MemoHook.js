import React, { useMemo, useState } from 'react';
import { procesoPesado } from '../../helpers/procesoPesado';
import { useCounter } from '../../hooks/useCounter';

export const MemoHook = () => {

    const { counter, increment } = useCounter(50);
    const [show, setShow] = useState(true);

    // Memoriza el estado del counter(depende), para no tener que
    // hacer un redibujado del dom
    const memoProcesoPesado = useMemo(() => procesoPesado(counter), [counter])

    return (
        <div className="container">
            <div className="row mt-3">
                <div className="col-md-8">
                    <h1>MemoHook</h1>
                    <hr />


                    <h3>Counter: <small>{counter}</small>  </h3>
                    <hr />

                    <p> {memoProcesoPesado} </p>

                    <button
                        className="btn btn-primary"
                        onClick={increment}
                    >
                        +1
                    </button>


                    <button
                        className="btn btn-outline-primary ml-3"
                        onClick={() => {
                            setShow(!show);
                        }}
                    >
                        Show/Hide {JSON.stringify(show)}
                    </button>
                </div>
            </div>
        </div>
    )
}
