import React, { useState } from 'react';
import { useCounter } from '../../hooks/useCounter';
import { Small } from './Small';

export const Memorize = () => {

    const { counter, increment } = useCounter(10);
    const [show, setShow] = useState(true);

    return (
        <div className="container">
            <div className="row mt-3">
                <div className="col-md-8">
                    <h1>Memorize</h1>
                    <hr />


                    <h3>Counter: <Small value={counter} />  </h3>
                    <hr />

                    <button
                        className="btn btn-primary"
                        onClick={increment}
                    >
                        +1
                    </button>


                    <button
                        className="btn btn-outline-primary ml-3"
                        onClick={() => {
                            setShow(!show);
                        }}
                    >
                        Show/Hide {JSON.stringify(show)}
                    </button>
                </div>
            </div>
        </div>
    )
}
