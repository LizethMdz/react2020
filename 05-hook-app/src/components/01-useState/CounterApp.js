import React, { useState } from 'react';

export const CounterApp = () => {

    const [state, setState] = useState({ counter1: 10, counter2: 20 });
    const { counter1, counter2 } = state;

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-8 m-auto">
                    <h1>Counter {counter1}</h1>
                    <h1>Counter {counter2}</h1>
                    <hr />
                    <button
                        className="btn btn-outline-primary"
                        onClick={() => {
                            setState({
                                ...state,
                                counter1: counter1 + 1
                            })
                        }}
                    >
                        Click +1
                    </button>
                </div>
            </div>

        </div>
    )
}
