import React from 'react'
import { useCounter } from '../hooks/useCounter'

export const CounterWithCustomHook = () => {

    const { state, increment, decrement, resetState } = useCounter(100);

    return (
        <div className="row">
            <div className="col-md-8">
                <h1>Counter with Hook: {state}</h1>
                <button onClick={() => increment(2)} className="btn btn-outline-success">+1</button>
                <button onClick={() => decrement(2)} className="btn btn-outline-success">-1</button>
                <button onClick={resetState} className="btn btn-outline-success">Reset</button>
            </div>

        </div>
    )
}
