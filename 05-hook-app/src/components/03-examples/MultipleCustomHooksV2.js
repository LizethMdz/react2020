import React from 'react';
import { useFetch } from "../../hooks/useFetch";
import { useCounter } from '../../hooks/useCounter';

export const MultipleCustomHooks = () => {
    const { counter, increment } = useCounter();
    const { loading, data } = useFetch(`https://www.breakingbadapi.com/api/quotes/${counter}`);
    // !!null Transforma el data a un false, porque si es !null = true
    const { author, quote } = !!data && data[0];

    return (
        <div className="container">
            <div className="row mt-3">
                <div className="col-md-8">
                    <h1>BreakingBad Quotes</h1>
                    <hr />

                    {
                        loading
                            ?
                            (
                                <div className="alert alert-info text-center">
                                    Loading...
                                </div>
                            )
                            :
                            (
                                <blockquote className="blockquote text-right">
                                    <p className="mb-0"> {quote} </p>
                                    <footer className="blockquote-footer"> {author} </footer>
                                </blockquote>
                            )
                    }

                    <button
                        className="btn btn-primary"
                        onClick={increment}
                    >
                        Siguiente quote {counter} ***
            </button>

                </div>
            </div>
        </div>
    )

}