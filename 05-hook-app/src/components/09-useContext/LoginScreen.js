import React, { useContext } from 'react'
import { UserContext } from './UserContext';

export const LoginScreen = () => {

    const { setUser } = useContext(UserContext);

    return (
        <div>
            <h1>Login Screen</h1>
            <button
                type="button"
                onClick={() => setUser({
                    id: 123,
                    name: 'lizeth'
                })}
                className="btn btn-primary btn-lg btn-block" >
                Login
            </button>
        </div>
    )
}
