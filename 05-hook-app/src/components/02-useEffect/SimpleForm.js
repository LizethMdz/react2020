import React, { useEffect, useState } from 'react'
import { Message } from './Message';

export const SimpleForm = () => {

    const [formState, setFormState] = useState({
        name: '',
        email: ''
    });

    const { name, email } = formState;

    useEffect(() => {
        console.log('escucha')
    }, []);

    useEffect(() => {
        console.log('onCHANGE');
    }, [formState]);

    useEffect(() => {
        console.log('onCHANGEEMAIL');
    }, [email]);

    const handleInputChange = ({ target }) => {
        setFormState({
            ...formState,
            [target.name]: target.value
        })
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-8">
                    <h3>useEffect</h3>
                    <hr />

                    <div className="form-group">
                        <input
                            type="text"
                            name="name"
                            className="form-control"
                            placeholder="Tu nombre"
                            autoComplete="off"
                            value={name}
                            onChange={handleInputChange} />
                    </div>

                    <div className="form-group">
                        <input
                            type="text"
                            name="email"
                            className="form-control"
                            placeholder="Tu email"
                            autoComplete="off"
                            value={email}
                            onChange={handleInputChange} />
                    </div>

                    {(name === '123') && <Message />}


                </div>
            </div>

        </div>
    )
}
