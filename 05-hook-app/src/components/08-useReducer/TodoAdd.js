import React from 'react'
import { useForm } from '../../hooks/useForm';

export const TodoAdd = ({ handleAddTodo }) => {

    const [{ description }, handleInputValue, reset] = useForm({
        description: ''
    });

    // State and Action will send to Reducer
    const handleSubmit = (e) => {
        e.preventDefault();

        if (description.trim().length <= 1) {
            return;
        }

        const newTodo = {
            id: new Date().getTime(),
            desc: description,
            done: false
        }
        // Sending data 
        handleAddTodo(newTodo);
        reset();
    }

    return (
        <div className="container">
            <h4>Agregar TODO</h4>
            <hr />
            <form onSubmit={handleSubmit}>
                <div className="form-group row">
                    <div className="col-sm-1-12">
                        <input
                            type="text"
                            className="form-control"
                            name="description"
                            value={description}
                            placeholder="Tarea.."

                            onChange={handleInputValue} />
                    </div>
                </div>
                <div className="form-group row">
                    <div
                        className="offset-sm-2 col-sm-10">
                        <button
                            type="submit"
                            className="btn btn-outline-primary">
                            Action
                                    </button>
                    </div>
                </div>
            </form>
        </div>
    )
}
