
// 1. Declarar el estado
const initialState = [{
    id: 1,
    todo: 'Comprar pan',
    done: false
}];
// 2. Mandar el state y el action
const todoReducer = (state = initialState, action) => {

    // 3. Modificar el state
    if (action?.type === 'agregar') {
        // Ahora si hace la inserción
        return [...state, action.payload];
    }
    // 4. Regresar mi estado
    return state;
}

let todos = todoReducer();

// Nuevo elemento
const newTodo = {
    id: 2,
    todo: 'Comprar leche',
    done: false
}

// Accion
const agregarTodoAction = {
    // Standard
    type: 'agregar',
    payload: newTodo // newTodo
}
// Before State and action 
todos = todoReducer(todos, agregarTodoAction);

console.log(todos);


