import React from 'react'

export const TodoListItem = ({ todo, index, handleDelete, handleToggle }) => {
    return (
        <div
            key={todo.id}
            className="list-group-item">
            <p
                onClick={() => handleToggle(todo.id)}
                className={`${todo.done && 'complete'}`}>
                {index + 1}. {todo.desc}
            </p>
            <button
                className="btn btn-outline-danger"
                onClick={() => handleDelete(todo.id)}>
                Borrar
            </button>
        </div>
    )
}
