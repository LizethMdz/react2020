import React, { useReducer, useEffect } from 'react';
import { todoReducer } from './todoReducer';

import { TodoList } from './TodoList';
import { TodoAdd } from './TodoAdd';

import './style.css';

// 1. Initial state
// const initialState = [{
//     id: new Date().getTime(),
//     desc: 'Aprender React',
//     done: false
// }];

const init = () => {
    // Recupera la información previamente almacenada asignando(initialState)
    return JSON.parse(localStorage.getItem('todos')) || [];
}

export const TodoApp = () => {

    // Ejecuta acciones en los TODO
    // Parámetros const [state, dispatch] = useReducer(reducer, initialState, init)
    const [todos, dispatch] = useReducer(todoReducer, [], init);

    useEffect(() => {
        // Si hubo una modificación en las tareas almacena la data
        localStorage.setItem('todos', JSON.stringify(todos));
    }, [todos]);

    const handleDelete = (todoId) => {

        const action = {
            type: 'delete',
            payload: todoId
        }

        dispatch(action);
    }

    const handleToggle = (todoId) => {
        dispatch({
            type: 'toogle',
            payload: todoId
        });
    }

    const handleAddTodo = (newTodo) => {

        dispatch({
            type: 'add',
            payload: newTodo
        });

    }
    //console.log(todos);


    return (
        <div className="container">
            <h1>TodoApp ({todos.length}) </h1>
            <hr />

            <div className="row m-0">
                <div className="col-md-7">
                    <TodoList
                        todos={todos}
                        handleDelete={handleDelete}
                        handleToggle={handleToggle}
                    />
                </div>


                <div className="col-md-5">
                    {/* Inicio form */}
                    <TodoAdd
                        handleAddTodo={handleAddTodo}
                    />
                    {/* Fin form */}
                </div>
            </div>
        </div>
    )
}