import { renderHook, act } from '@testing-library/react-hooks';
import { useCounter } from '../../hooks/useCounter';
import '@testing-library/jest-dom';

describe('Pruebas en useCounter', () => {
    test('Debe de retornar valores por defecto', () => {
        const { result } = renderHook(() => useCounter());
        expect(result.current.counter).toBe(10);
        expect(typeof result.current.increment).toBe('function');
        expect(typeof result.current.decrement).toBe('function');
        expect(typeof result.current.reset).toBe('function');
    });
    test('Debe de tener el counter en 100', () => {
        const { result } = renderHook(() => useCounter(100));
        expect(result.current.counter).toBe(100);
    });
    test('Debe de incrementar el counter en 1', () => {
        const { result } = renderHook(() => useCounter(100));

        const { increment } = result.current;

        /**Para poder ejecutar las funciones individuales del hook de usa act() */
        act(() => {
            increment();
        });

        expect(result.current.counter).toBe(101);
    });
    test('Debe de decrementar el counter en 1', () => {
        const { result } = renderHook(() => useCounter(100));

        const { decrement } = result.current;

        /**Para poder ejecutar las funciones individuales del hook de usa act() */
        act(() => {
            decrement();
        });

        expect(result.current.counter).toBe(99);
    });
    test('Debe de resetear el valor original del counter', () => {
        const { result } = renderHook(() => useCounter(100));

        const { increment, reset } = result.current;

        /**Para poder ejecutar las funciones individuales del hook de usa act() */
        act(() => {
            increment();
            reset();
        });

        expect(result.current.counter).toBe(100);
    });

});