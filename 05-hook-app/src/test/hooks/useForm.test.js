import { renderHook, act } from '@testing-library/react-hooks';
import { useForm } from '../../hooks/useForm';
import '@testing-library/jest-dom';

describe('Pruebas en useForm', () => {
    const initialForm = {
        name: 'lizeth',
        email: 'liz@mail.com'
    }
    test('Debe de retornar un formulario por defecto', () => {
        const { result } = renderHook(() => useForm(initialForm));
        const [values, handleInputChange, reset] = result.current;
        //console.log(result.current);
        expect(values).toEqual(initialForm);
        expect(typeof handleInputChange).toBe('function');
        expect(typeof reset).toBe('function');
    });
    test('Debe de cambiar el valor del formulario (cambiar name)', () => {
        const { result } = renderHook(() => useForm(initialForm));
        const [, handleInputChange] = result.current;
        const inputName = {
            target: {
                name: 'name',
                value: 'jose'
            }
        };
        act(() => {
            handleInputChange(inputName);
        });

        // console.log(result.current);
        const [values] = result.current;
        expect(values).toEqual({ ...initialForm, name: 'jose' });
        expect(values.name).toBe('jose');
    });
    test('Debe de re-establecer el formulario con RESET', () => {
        const { result } = renderHook(() => useForm(initialForm));
        const [, handleInputChange, reset] = result.current;
        const inputName = {
            target: {
                name: 'name',
                value: 'jose'
            }
        };
        act(() => {
            handleInputChange(inputName);
            reset();
        });

        // console.log(result.current);
        const [values] = result.current;
        //console.log(values.name);
        expect(values).toEqual(initialForm);
    });
});