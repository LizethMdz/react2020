import React from 'react';
import { mount } from 'enzyme';
import { HomeScreen } from '../../../components/09-useContext/HomeScreen';
import { UserContext } from '../../../components/09-useContext/UserContext';


describe('Pruebas en <HomeScreen />', () => {

    const user = {
        name: 'Fernando',
        email: 'fernando@gmail.com'
    }


    test('debe de mostrarse correctamente', () => {
        // const wrapper = shallow(
        //     <UserContext.Provider value={{
        //         user
        //     }}>
        //         <HomeScreen />
        //     </UserContext.Provider>
        // );
        const wrapper = mount(
            <UserContext.Provider value={{
                user
            }}>
                <HomeScreen />
            </UserContext.Provider>
        );

        expect(wrapper).toMatchSnapshot();

    });


})
