import React from 'react';
import { shallow } from 'enzyme';
import { TodoListItem } from '../../../components/08-useReducer/TodoListItem';
import { demoTodos } from '../../fixtures/demoTodos';

describe('Pruebas en <TodoListItem />', () => {

    const handleDelete = jest.fn();
    const handleToggle = jest.fn();

    let wrapper = shallow(
        <TodoListItem
            todo={demoTodos[0]}
            index={0}
            handleDelete={handleDelete}
            handleToggle={handleToggle}
        />
    );

    test('Debe de mostrarse correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    });
    test('Debe de llamar la función handleDelete', () => {
        const button = wrapper.find('button');
        button.simulate('click');
        expect(handleDelete).toBeCalledWith(demoTodos[0].id);
    });
    test('Debe de llamar la función handleToggle', () => {
        const texto = wrapper.find('p');
        texto.simulate('click');
        expect(handleToggle).toBeCalledWith(demoTodos[0].id);
    });
    test('Debe de mostrar el texto correctamente', () => {
        const texto = wrapper.find('p').text().trim();
        expect(texto).toBe(`1. ${demoTodos[0].desc}`);

    });

    test('Debe de tener la clase complete si el TODO.done = true', () => {
        const todo = demoTodos[0];
        todo.done = true;

        const wrapper = shallow(
            <TodoListItem
                todo={todo}
            />
        );

        expect(wrapper.find('p').hasClass('complete')).toBe(true);
    });

});