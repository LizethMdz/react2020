import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';

import { HookApp } from '../HookApp';

describe('Pruebas en el componente HookApp', () => {
    test('Debe mostrar el HookApp correctamente', () => {
        const wrapper = shallow(<HookApp />);
        expect(wrapper).toMatchSnapshot();
    });
});