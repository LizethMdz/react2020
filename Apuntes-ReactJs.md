# ¿Qué es React?

- Libreria
- Declarativa
- Eficiente
- Predecible
- Componentes
- Server-side con Node
- Aplicaciones móviles

========================== PROPTYPES ==============================

- URL https://es.reactjs.org/docs/typechecking-with-proptypes.html

======================= PRUEBAS ===================================

### Introducción pruebas

**¿Qué son las pruebas?**

1. Unitarias - Enfocadas en pequeñas funcionalidades
1. Integración - Enfocadas en cómo reaccionan varias piezas en conjunto

### Características

1. Fáciles de escribir
1. Fáciles de leer
1. Confiables
1. Rápidas
1. Principalmente

### TESTING A - A - A

**_Describe a continuación_**

**Arrange** --- _Preparamos el estado inicial_

- Inicializamos variables
- Importaciones necesarias

**Act** --- _Aplicamos estimulos o acciones_

- LLamar métodos
- Simular clicks
- Realizar acciones sobre el paso anterior

**Assert** --- _Observar el comportamiento resultante_

- Son los resultados esperados
- Ej. Que algo cambie, algo incremente o bien que nada suceda

### Temas Generales

- Arrange - Arreglar
- Act - Actuar
- Assert - Afirmar
- JEST
- EXPECT
- toBe()
- ENZYME
- Simular eventos

**_Comando para correr las pruebas_**

COMMAND PROMPT `npm run test`

**_Comando para correr las pruebas de un solo archivo_**

CODE ACCEPTED p (aption) -> src (filename) -> select(arrow-down)

### ENZYME PARA TESTING DE COMPONENTES

- URL https://github.com/enzymejs/enzyme

COMMAND PROMPT `npm i --save-dev enzyme enzyme-adapter-react-16`

**_*Se configura en el archivo setupTest.js*_**

1. Paso 1:

   - import Enzyme from 'enzyme';
   - import { configure } from 'enzyme'; (Opcion que actualmente funciona mejor)
   - import Adapter from 'enzyme-adapter-react-16';

1. Paso 2:
   - Enzyme.configure({ adapter: new Adapter() });

**_*Se necesita instalar otra libreria para poder crear un snapshot del componenete*_**

_También se agrega al setupTest.js_

1. Paso 3:
   - import { createSerializer } from 'enzyme-to-json';
   - configure({ adapter: new Adapter() }); (Recomendada)
   - expect.addSnapshotSerializer(createSerializer({ mode: 'deep' }));

COMMAND PROMPT `npm install --save-dev enzyme-to-json`

**_*Instalar otra libreria para el testeo de un hook*_**

- URL https://react-hooks-testing-library.com/usage/basic-hooks

COMMAND PROMPT `npm install --save-dev @testing-library/react-hooks`

**_*NOTA IMPORTANTE - SI HAY ERROR*_**

- URL https://enzymejs.github.io/enzyme/docs/installation/react-16.html

COMMAND PROMPT `npm i --save react@16 react-dom@16`
======================= DESPLIEGUE SENCILLO ===================================

### Como generar el build de producción del proyecto.

COMMAND PROMPT `npm run build`

- NPM HTTP:SERVER // Desplegar un servidor rápido, para validar su funcionamiento
- COMMAND PROMPT `npm i --global http-server`

======================= REDUCER ==============================================

## ¿Qué es un REDUCER?

1. Es una función común y corriente
1. Debe ser una funció pura

   - No debe de tener efectos secundarios
   - No debe de realizar tareas asícronas
   - Debe de retornar siempre un estado nuevo
   - No debe de llamar localStorage o sessionStorage
   - No debe de requerir más que una acción que puede tener un argumento

1. Debe retornar un nuevo estado
1. Recibe dos argumentos
   - El valor inicial(initialSate) y la accion a ejecutar

### Pruebas en Reducer

**_*Importante:*_**

1. Considerar el action
1. Considerar el initialState de salida

**_*Ejemplo de código:*_**

```javascript
test("Debe de autenticar y colocar el name del usuario", () => {
  const action = {
    payload: {
      name: "lizeth",
    },
    type: types.login,
  };

  const state = authReducer({ logged: false }, action);

  expect(state).toEqual({ name: "lizeth", logged: true });
});
```

======================= REDUX ==============================================

## ¿Qué es Redux ?

1. Redux
   - Es un contenedor predecible del estado de nuestra aplicación
1. Store
   - Fuente única de la verdad, Ahi es donde esta la informacion que mis componentes consumirán
1. Middlewares
1. Dispatch
1. Actions
1. State
1. Acciones asíncronas

**_*Instalación de React Redux*_**

COMMAND PROMPT `npm install react-redux`

**_*Documentación*_**

- URL https://react-redux.js.org/introduction/quick-start

- URL https://redux.js.org/basics/store

- URL https://es.redux.js.org/

**_*Seguir la documentación, para visualizar Redux extention en Navegadores*_**

- URL https://github.com/zalmoxisus/redux-devtools-extension#usage

**_*Instalación de Redux-TRhunk*_**

COMMAND PROMPT `npm install --save redux-thunk`

**_*Instalación de Redux-TRhunk*_**

COMMAND PROMPT `npm i firebase`

**_*Intalación de Mock para pruebas*_**

COMMAND PROMPT `npm install redux-mock-store --save-dev`

- URL https://github.com/reduxjs/redux-mock-store
