// export const heroes  - Exportacion que se haría de manera normal

const heroes = [
    {
        id: 1,
        name: 'Batman',
        owner: 'DC'
    },
    {
        id: 2,
        name: 'Spiderman',
        owner: 'Marvel'
    },
    {
        id: 3,
        name: 'Superman',
        owner: 'DC'
    },
    {
        id: 4,
        name: 'Flash',
        owner: 'DC'
    },
    {
        id: 5,
        name: 'Wolverine',
        owner: 'Marvel'
    },

];

// Exportacion normal
// export const casas = ['DC', 'MARVEL'];
const casas = ['DC', 'MARVEL'];

// Exportacion default
// export default herores;

// Exportaciones conjuntas
// Exportación con un elemento como default
export {
    heroes as default, casas
}