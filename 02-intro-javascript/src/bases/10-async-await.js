const apiKey = 'lRochspvszcBJdF1iKFtZDscZgNpvZWx';

const urlApi = `http://api.giphy.com/v1/gifs/random?api_key=${apiKey}`;

const getImagenPromesa = () => new Promise(resolve => resolve(urlApi));
getImagenPromesa().then(console.log);

const getImagenPromesa2 = async () => {

    try {
        const resultado = await fetch(urlApi);
        const { data } = await resultado.json();
        //console.log(data);
        const { url } = data.images.original;
        const img = document.createElement('img');
        img.src = url;
        document.body.appendChild(img);

    } catch (err) {
        console.log(err);
    }

}

console.log(getImagenPromesa2());