// ================== Objetos loterales

const persona = {
    nombre: 'lizeth',
    apellido: 'mendoza',
    edad: 23,
    direccion: {
        calle: 'democracia',
        ciudad: 'qro',
        codigo: 1245,
    }
}


//Copiamos la misma referencia a esa persona en memoria
const persona2 = persona;

persona2.nombre = 'peter';

console.log(persona);
console.log(persona2);

// Mejor con el operador spred
const persona3 = { ...persona };
console.log(persona3);