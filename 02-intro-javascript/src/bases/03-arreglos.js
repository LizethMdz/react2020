
// ============== Arreglos
// Solo con arreglos predefinidos, no es muy utilizada
//const arreglo = new Array();
// Forma tradicional
const arr = [1, 2, 3];
// arr.push(1);
// arr.push(2);
// arr.push(3);

// No es lo mismo
let arreglo2 = [arr, 5];
//arreglo2.push(5);
//Crea un copia enlazando los elementos en la misma posicion
let arreglo3 = [...arr, 5];
let arreglo4 = arreglo3.map(function (numero) {
    return numero * numero;
});

//console.log(arreglo);
console.log(arr);
console.log(arreglo2);
console.log(arreglo3);
console.log(arreglo4);