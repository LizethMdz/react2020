// ============== Desestructuración de arreglos

const personajes = ['goku', 'vegeta', 'trunks'];

const [goku] = personajes;

// Si quiero solo la segunda posicion, se deja el espacio como tal
const [, vegeta] = personajes;

console.log(goku, vegeta);

const retornaArreglos = () => {
    return ['ABC', 123];
}

const [letras, numeros] = retornaArreglos();

console.log(letras, numeros);


// Tarea
const useState = (valor) => {
    return [valor, () => { console.log('hola!!') }];
}

// Asignar el return a una nueva constante
const arr = useState('goku');
console.log(arr);
// La segunda propiedad tiene asignada un valor como función, en este caso no se 
// agregan parentesis, porque no es válido, además de que, no es necesario ejecutarla
// en ese mismo momento.
const [nombre, setNombre] = useState('vegeta');

console.log(nombre);
setNombre(); // arr[1]() - También es válido, pero no usualmente visto 


