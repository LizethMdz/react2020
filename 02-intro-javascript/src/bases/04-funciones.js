// ========== Functions

const saludar = function saludar(nombre) {
    return `Hola, ${nombre}`;
}

const saludar2 = (nombre) => {
    return `Hola, ${nombre}`;
}
const saludar3 = (nombre) => `Hola, ${nombre}`;

console.log(saludar('persona1 XD'));
console.log(saludar2('persona2 XD'));
console.log(saludar3('persona3 XD'));

// Regresar un objeto con la palabra return
const getUser = () => {
    return {
        uid: 'sdsad123',
        username: 'ejemp123'
    }
}

// Regresar un objeto sin la palabra return
const getUser2 = () =>
    ({
        uid: 'sdsad123',
        username: 'ejemp123'
    })


console.log(getUser());
console.log(getUser2());

// Tarea

function getUserActivo(nombre) {
    return {
        uid: 'ABCD123',
        username: nombre
    }
}

const usuarioAct = getUserActivo('noel');
console.log(usuarioAct);

// Transformación

const getUsuarioAct = (nombre) =>
    ({
        uid: 'ABCD123',
        username: nombre
    })

console.log(getUsuarioAct('luz'));