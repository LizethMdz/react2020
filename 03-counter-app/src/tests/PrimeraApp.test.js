import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';
//import { render } from '@testing-library/react';
import PrimeraApp from '../PrimeraApp';

describe('Pruebas en el componente PrimeraApp', () => {
    // test('Debe mostrar PrimeraApp correctamente ', () => {
    //     const saludo = 'Soy Yuki';
    //     const { getByText } = render(<PrimeraApp saludos={saludo} />);
    //     Se necesitó crear el archivo setupTest.js - con una importación
    //     expect(getByText(saludo)).toBeInTheDocument();
    // });
    test('Debe mostrar PrimeraApp correctamente', () => {
        const saludo = 'Soy Yuki';
        const wrapper = shallow(<PrimeraApp saludos={saludo} />);
        expect(wrapper).toMatchSnapshot();
    });

    test('Debe mostrar el subtitulo enviando por props', () => {
        const saludo = 'Soy Yuki';
        const subTitulo = 'Soy un subtitulo';
        const wrapper = shallow(
            <PrimeraApp
                saludos={saludo}
                subtitulo={subTitulo}
            />
        );
        const textoParrafo = wrapper.find('p').text(); //funciona como un queryselector
        expect(textoParrafo).toBe(subTitulo);
    });


});