import React from 'react';
import { shallow } from 'enzyme';
import '@testing-library/jest-dom';
import CounterApp from '../CounterApp';

describe('Pruebas en el componente CounterApp', () => {
    let wrapper = shallow(<CounterApp />);
    beforeEach(() => {
        wrapper = shallow(<CounterApp />);
    });
    test('Debe mostrar CounterApp correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    });
    test('Debe mostrar el value mandado por props', () => {
        const wrapper = shallow(<CounterApp value={100} />);
        const valorDado = wrapper.find('h2').text().trim();
        expect(valorDado).toBe('100');
    });

    it('Debe de incrementar si se presiona en un boton', () => {
        wrapper.find('button').at(0).simulate('click');
        const valorDado = wrapper.find('h2').text().trim();
        expect(valorDado).toBe('11');
    });

    it('Debe de decrementar si se presiona en un boton', () => {
        wrapper.find('button').at(2).simulate('click');
        //console.log(btn);
        const valorDado = wrapper.find('h2').text().trim();
        expect(valorDado).toBe('9');
    });

    it('Debe de colocar por defecto si se presiona (reset)', () => {
        const wrapper = shallow(<CounterApp value={105} />);
        wrapper.find('button').at(0).simulate('click');
        wrapper.find('button').at(0).simulate('click');
        wrapper.find('button').at(1).simulate('click');
        //console.log(btn);
        const valorDado = wrapper.find('h2').text().trim();
        expect(valorDado).toBe('105');
    });
});