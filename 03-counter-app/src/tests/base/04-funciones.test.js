import '@testing-library/jest-dom';
import { getUser, getUserActivo } from '../../bases/04-funciones';

describe('Pruebas en 04-funciones', () => {
    test('Prueba en el método getUser(), debe regresar un objeto', () => {
        const usuario = {
            uid: 'sdsad123',
            username: 'ejemp123'
        }

        const resp = getUser();
        // Para comparar dos objetos
        expect(resp).toEqual(usuario);
    });

    test('Prueba en el método getUserActivo(), debe regresar un objeto', () => {

        const nombre = 'lizeth';

        const usuario = {
            uid: 'ABCD123',
            username: nombre
        }

        const resp = getUserActivo(nombre);
        // Para comparar dos objetos
        expect(resp).toEqual(usuario);
    });
}); 