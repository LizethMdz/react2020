//Ayuda para el autoacompletado
import '@testing-library/jest-dom';
import { getSaludo } from '../../bases/01-template-string';

describe('Pruebas en 02-template-string', () => {
    test('Prueba en el método getSaludo(), debe retornar Hola mundo lizeth', () => {
        const nombre = 'fernando';
        const resp = getSaludo(nombre);
        expect(resp).toBe('Hola mundo ' + nombre);
    })
    test('Prueba en el método getSaludo(), debe retornar Hola mundo Carlos', () => {
        const resp = getSaludo();
        //console.log(resp);
        expect(resp).toBe('Hola mundo Carlos');
    })

})