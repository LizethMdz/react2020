import '@testing-library/jest-dom';
import { getImagenPromesa2 } from '../../bases/10-async-await';

describe('Pruebas en 10-async-await', () => {
    test('Prueba en el método getImagenPromesa2(), debe regresar un heroe por id', async () => {
        const url = await getImagenPromesa2();
        expect(url.includes('https://')).toBe(true);
    });
});