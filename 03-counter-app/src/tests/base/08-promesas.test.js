import '@testing-library/jest-dom';
import { getHeroeByIdAsync } from '../../bases/08-promesas';
import heroes from '../../data/herores';


describe('Pruebas en 08-promesas', () => {
    test('Prueba en el método getHeroeByIdAsync(), debe regresar un heroe por id', (done) => {
        const id = 1;
        getHeroeByIdAsync(id).then(
            heroe => {
                expect(heroe).toBe(heroes[0]);
                done();
            }
        )
    });
    test('Prueba en el método getHeroeByIdAsync(), no debe regresar un heroe por id', (done) => {
        const id = 10;
        getHeroeByIdAsync(id).catch(e => {
            expect(e).toBe('No se pudo llevar a cabo');
            done();
        });
    });
});