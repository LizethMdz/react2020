import '@testing-library/jest-dom';
import { getHeroeById, getHeroesByOwner2 } from '../../bases/07-import-exp';
import heroes from '../../data/herores';

describe('Pruebas en 07-import-exp', () => {
    test('Prueba en el método getHeroeById(), debe regresar un heroe por id', () => {
        const id = 1;
        const heroe = getHeroeById(id);
        const heroesData = heroes.find(h => h.id === id);
        expect(heroe).toEqual(heroesData);
    });
    test('Prueba en el método getHeroeById(), debe regresar undefined si no hay heroe', () => {
        const id = 10;
        const heroe = getHeroeById(id);
        expect(heroe).toEqual(undefined);
    });
    test('Prueba en el método getHeroesByOwner2(), debe de regresar un arreglo de con heroes DC', () => {
        const owner = 'DC';
        const heroes_ = getHeroesByOwner2(owner);
        const heroesData = heroes.filter(h => h.owner === owner);
        expect(heroes_).toEqual(heroesData);
    });
    test('Prueba en el método getHeroeById(), debe de regresar un arreglo de heroes MARVEL', () => {
        const owner = 'Marvel';
        const heroes_ = getHeroesByOwner2(owner);
        expect(heroes_.length).toBe(2);
    });
});