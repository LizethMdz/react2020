import '@testing-library/jest-dom';
import { retornaArreglos } from '../../bases/06-desestr-arr';

describe('Pruebas en 06-desestr-arr', () => {
    test('Prueba en el método retornaArreglos(), debe regresar un arreglo', () => {
        // Forma 1
        //const resp = retornaArreglos();
        //expect(resp).toEqual(['ABC', 123]);
        // Forma 2
        const [letras, numeros] = retornaArreglos();
        expect(letras).toEqual('ABC');
        expect(typeof letras).toBe('string');
    });
});