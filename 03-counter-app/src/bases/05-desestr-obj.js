// ================== Desestructuracion
const persona = {
    nombre: 'lizeth',
    apellido: 'mendoza',
    edad: 23,
    direccion: {
        calle: 'democracia',
        ciudad: 'qro',
        codigo: 1245,
    }
}
// Desestructuracion normal
// const { nombre, edad, apellido } = persona;

// console.log(nombre);

// Desestructuracion con funciones normal

const retornaPersona = (usuario) => {
    const { nombre, edad } = usuario;
    console.log(nombre, edad);
}
retornaPersona(persona);


// Desestructuracion con funciones desde los parametros
const retornaPersona2 = ({ nombre, edad, apellido = 'morales' }) => {

    console.log(nombre, edad, apellido);
}

retornaPersona2(persona);

const useContext = ({ nombre, edad, apellido }) => {

    return {
        nC: nombre,
        anios: edad, // V1.0
        latlng: {
            lat: 12.4545,
            lng: -12.8349
        } // V2.0
    }
}

const avenger = useContext(persona);
console.log(avenger);
//NOTE Los elementos desestructurados deben llamarse igual a los que retorna, cuando se renombran
// es decir, no se llaman igual propiedad : valor
const { nC, anios } = useContext(persona);
console.log(nC, anios);

//Pero que pasa si queremos extraer propiedades del objeto regresado que contiene otro objeto

const { latlng: { lat, lng } } = useContext(persona);
console.log(lat, lng);