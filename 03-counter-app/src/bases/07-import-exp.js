// NOTE Esta e una importación normal
// import { heroes } from "./data/herores";
// NOTE Pero cuando hacemos una exportacion por default
// import heroes from './data/herores';
// NOTE Pero cuando hacemos una exportacion por default y un exportacion normal
//import heroes, { casas } from './data/herores';
// NOTE Para exportacion conjunta
// import { heroes, casas } from './data/herores';
// NOTE Para exportaciones conjuntas pero una de ellos as default
import heroes, { casas } from '../data/herores';

console.log(casas);

export const getHeroeById = (id) => {
    return heroes.find((elemento) => elemento.id === id);
}

//console.log(getHeroeById(2));
// Solo trae un elemento
//const getHeroesByOwner = (owner) => heroes.find(heroe => heroe.owner === owner);
// Trae a todos los que cumplan la comdición
export const getHeroesByOwner2 = (owner) => heroes.filter(heroe => heroe.owner === owner);

//console.log(getHeroesByOwner('DC'));
//console.log(getHeroesByOwner2('DC'));