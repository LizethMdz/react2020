const apiKey = 'lRochspvszcBJdF1iKFtZDscZgNpvZWx';

const urlApi = `http://api.giphy.com/v1/gifs/random?api_key=${apiKey}`;

// const getImagenPromesa = () => new Promise(resolve => resolve(urlApi));
// getImagenPromesa().then(console.log);

export const getImagenPromesa2 = async () => {

    try {
        const resultado = await fetch(urlApi);
        const { data } = await resultado.json();
        const { url } = data.images.original;
        return url;
        // const img = document.createElement('img');
        // img.src = url;
        // document.body.appendChild(img);

    } catch (err) {
        return 'No existe';
    }

}

//console.log(getImagenPromesa2());