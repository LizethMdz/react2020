// Importación
import { getHeroeById } from './07-import-exp';

// =================== Promesas
// const promesa = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     console.log('2 min later');
//     // Tarea importar la funcion getHeroeById
//     //resolve();
//     const heroe = getHeroeById(2);
//     //console.log(heroe);
//     //resolve(heroe);
//     reject('No se pudo llevar a cabo');
//   }, 2000);
// });

// promesa.then((heroe) => {
//   console.log('Heroe ', heroe);
// })
//   .catch(err => console.warn(err));

export const getHeroeByIdAsync = (id) => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            //console.log('2 min later');
            // Tarea importar la funcion getHeroeById
            //resolve();
            const heroe = getHeroeById(id);
            //console.log(heroe);
            if (heroe !== undefined) {
                resolve(heroe);
            } else {
                reject('No se pudo llevar a cabo');
            }
        }, 1500);
    });
}




