// Functional components
//import React, { Fragment } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
const PrimeraApp = ({ saludos, subtitulo }) => {

    const saludo = 'const';
    // NOTE Podemos imprimir valores primitivos, pero tenemos unos casos especiales
    // Los arreglos se muestran de forma cocatenada
    // Imprimir objetos literalmente no es posible
    const objet = {
        nombre: 'lizeth',
        edad: 23
    } // Para mostrarlo debemos usar JSON.stringigy()
    return (
        <>
            <h1>{saludos}</h1>
            <h3>Con Props en el Componente Hola Mundo {saludos}</h3>
            <pre>Hola Mundo {JSON.stringify(objet, null, 3)}</pre>
            <p>{subtitulo}</p>
        </>
    )

}

// Obligar a que se respete mi uso de propiedades

PrimeraApp.propTypes = {
    //Es un string  pero no obligatorio
    //saludos: PropTypes.string
    // En este caso si es obligatorio
    saludos: PropTypes.string.isRequired
}
PrimeraApp.defaultProps = {
    subtitulo: 'Soy un subtitulo'
}
export default PrimeraApp;
