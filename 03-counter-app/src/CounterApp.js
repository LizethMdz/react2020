//Snippet - rafcp

import React, { useState } from 'react'
import PropTypes from 'prop-types'

export const CounterApp = ({ value = 10 }) => {

    // Hooks
    const [counter, setCounter] = useState(value);

    const handleAdd = () => setCounter((c) => c + 1);

    const handleReset = () => setCounter((c) => c = value);

    const handleReduce = () => setCounter((c) => c - 1);

    return (
        <>
            <h1>CounterApp</h1>
            <h2> {counter} </h2>
            {/* Una forma de mandar el event(parámetro) a un handler */}
            {/* <button onClick={(e) => handleAdd(e)}>Sumar</button> */}
            <button onClick={handleAdd}>Sumar</button>
            <button onClick={handleReset}>Reset</button>
            <button onClick={handleReduce}>Restar</button>
        </>
    )
}

CounterApp.propTypes = {
    value: PropTypes.number
}

export default CounterApp;